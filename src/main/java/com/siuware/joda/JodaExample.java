package com.siuware.joda;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

public class JodaExample {
    public static void main(String[] args) {
        LocalDateTime currentDateAndTime = LocalDateTime.now();
        DateTime dateTime = currentDateAndTime.toDateTime();
        LocalDate localDate = currentDateAndTime.toLocalDate();
        LocalTime localTime = currentDateAndTime.toLocalTime();
        System.out.println("dateTime = " + dateTime);
        System.out.println("localDate = " + localDate);
        System.out.println("localTime = " + localTime);

    }
}
