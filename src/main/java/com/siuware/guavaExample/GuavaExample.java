package com.siuware.guavaExample;

import com.siuware.lombockExample.PersonExample;
import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
public class GuavaExample {

    public static int compareTo(PersonExample p1, PersonExample p2) {
        return ComparisonChain.start()
                .compare(p1.getForename(), p2.getForename())
                .compare(p1.getLastname(), p2.getLastname())
                .compare(p1.getAge(), p2.getAge())
                .result();
    }

    public static void joiner() {
        String s = Joiner.on("; ").skipNulls().join("Harry", null, "Ron", "Hermione", "Gandalf");
        System.out.println(s);
    }

    public static void joiner2() {
        String s = Joiner.on(",").join(Arrays.asList(1, 5, 7));
        System.out.println(s);
    }

    private static void splitter() {
        Iterable<String> s = Splitter.on(',').trimResults().omitEmptyStrings().split("foo,bar,,   qux");
        System.out.println(s);
    }

    // https://www.baeldung.com/guava-string-charmatcher

    private static void charMatcher1() {
        String input = "H*el.lo,}12";
        CharMatcher matcher = CharMatcher.javaLetterOrDigit();
        String s = matcher.retainFrom(input);
        System.out.println(s);
    }

    private static void charMatcher2() {
        String input = "H*el.lo,}12";
        CharMatcher matcher = CharMatcher.javaLetter();
        String s = matcher.retainFrom(input);
        System.out.println(s);
    }

    private static void charMatcher3() {
        String input = "H*el.lo,}12";
        CharMatcher matcher = CharMatcher.digit();
        String s = matcher.retainFrom(input);
        System.out.println(s);
    }

    private static void charMatcher4() {
        String input = "H*el .lo ,}12";
        CharMatcher matcher = CharMatcher.breakingWhitespace();
        String s = matcher.retainFrom(input);
        System.out.println(s.length());
    }

    private static void sort() {
        List<Integer> toSort = Arrays.asList(3, 5, 4, null, 1, 2);
        Collections.sort(toSort, Ordering.natural().nullsFirst());
        System.out.println(toSort);
    }

    public static void main(String[] args) {
        sort();
    }
}
