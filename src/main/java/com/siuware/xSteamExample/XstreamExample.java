package com.siuware.xSteamExample;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class XstreamExample {

    private int id;
    private String name;

    public static void main(String[] args) {
        XstreamExample xs = new XstreamExample(4616, "Cheng");
        XStream xstream = new XStream(new DomDriver());
        String xml = xstream.toXML(xs);
        System.out.println(xml);
    }
}
