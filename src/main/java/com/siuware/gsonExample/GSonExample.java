package com.siuware.gsonExample;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Data;

public class GSonExample {

    @AllArgsConstructor
    @Data
    static class NicePlace {
        private String city;
        private String country;
        private int zip;

        public String toString() {
            return zip + " " + city + ", " + country;
        }
    }


    public static void main(String[] args) {
        NicePlace obj = new NicePlace("Brühl", "Germany", 50321);
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        System.out.println(json);


        NicePlace obj2 = gson.fromJson(json, NicePlace.class);
        System.out.println(obj2);
    }
}
