package com.siuware.skiplist;

import java.util.Random;

public class Main {
    static int min = 1;
    static int max = 1000;

    public static void main(String[] args) {
        SkipList list = new SkipList();
        for (int i = 0; i < 10; i++) {
            Chain chain = new Chain(randInt(min, max));
            System.out.println("Start adding = " + chain.value);
            list.add(chain);
        }
        System.out.println("list.toString() = " + list.toString());
        System.out.println("list.size() = " + list.size());
    }

    public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

}
