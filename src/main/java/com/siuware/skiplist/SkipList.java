package com.siuware.skiplist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SkipList {

    Chain start;

    public SkipList() {

    }

    public void add(Chain chain) {
        if (getStart() == null) setStart(chain);
        else {
            getStart().add(chain);
            resetStart();
        }
    }

    public boolean remove(int value) {
        return start.remove();
    }

    public void resetStart() {
        if (start.left != null) start = start.getFirstChain();
    }

    @Override
    public String toString() {
        return start.toString();
    }

    public int size() {
        return Chain.sizeW;
    }
}
