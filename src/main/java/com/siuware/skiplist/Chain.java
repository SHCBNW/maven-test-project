package com.siuware.skiplist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Chain {
    static int sizeW = 1;
    static int sizeH = 1;
    Chain left;
    Chain right;
    Chain up;
    Chain down;
    Integer value;

    public Chain(int value) {
        this.value = value;
    }

    public void add(Chain chain) {
        if (chain.value.equals(this.value)) {
            System.out.println("1. Chain exist");
            // throw new Exception("Chain already exists.");
        } else if (chain.value < this.value) { // concat left
            System.out.println("2. concat left");
            this.left = chain;
            chain.right = this;
            sizeW++;
        } else if (this.right == null) { // concat right
            System.out.println("3. concat right");
            chain.left = this;
            this.right = chain;
            sizeW++;
        } else if (right.getValue() < chain.getValue()) { // move right
            right.add(chain);
        } else if (right.getValue() > chain.getValue()) { // concat between
            System.out.println("5. concat between");
            chain.right = right;
            chain.left = this;
            this.right = chain;
            sizeW++;
        }
    }

    Chain find(int value) {
        if (this.value == value) return this;
        if (this.right.value == value) return this.right;
        if (this.right.value > value) return this.down.find(value);
        return this;
    }

    int getValue() {
        if (this.value == null && this.down != null)
            return this.down.getValue();
        else if (this.value != null && this.down == null)
            return this.value;
        throw new NullPointerException("Something went wrong. Lowest chain has no value.");
    }

    int xSizeOnThisLevel() {
        int counter = 1;
        Chain discovery = this;
        while (discovery.getLeft() != null) {
            ++counter;
            discovery = discovery.getLeft();
        }
        discovery = this;
        while (discovery.getRight() != null) {
            ++counter;
            discovery = discovery.getRight();
        }
        return counter;
    }

    int ySizeOnThisLevel() {
        int counter = 1;
        Chain discovery = this;
        while (discovery.up != null) {
            ++counter;
            discovery = discovery.getUp();
        }
        discovery = this;
        while (discovery.down != null) {
            ++counter;
            discovery = discovery.getDown();
        }
        return counter;
    }

    Chain getFirstChain() {
        if (left != null) return left.getFirstChain();
        else return this;
    }

    Chain getLastChain() {
        if (right != null) return right.getLastChain();
        else return this;
    }

    @Override
    public String toString() {
        // System.out.println("I am " + value + ", right.isNull() = " + (this.right == null));
        if (this.right != null) {

            return "[" + this.value + "], " + right.toString();
        }
        return "[" + this.value + "]";
    }

    private String detailString() {
        String s = "" + this.value;
        return s;
    }

    public boolean remove(int value) {
        if (down != null) {
            System.out.println("fastline level");
        } else { // lowest level
            if (this.value == value && right == null) { // remove last

            } else if (right.value == value) { // remove between
                left.right = right;
                right.left = left;
                return true;
            } else if (right.value < value) {
                return false;
            } else {// move right
                return right.remove(value);
            }
        }
        return false;
    }
}
