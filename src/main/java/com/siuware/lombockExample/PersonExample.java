package com.siuware.lombockExample;


import lombok.AllArgsConstructor;
import lombok.Data;

@SuppressWarnings("SpellCheckingInspection")
@AllArgsConstructor
@Data
public class PersonExample {
    private String forename;
    private String lastname;
    private int age;

}
