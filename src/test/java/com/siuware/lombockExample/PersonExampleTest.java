package com.siuware.lombockExample;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PersonExampleTest {

    PersonExample p1;

    @Before
    public void setUp() throws Exception {
        p1 = new PersonExample("Siu", "Cheng", 31);
    }

    @Test
    public void getForename() {
        Assert.assertSame(p1.getForename(), "Siu");
    }

    @Test
    public void getLastname() {
        Assert.assertSame(p1.getLastname(), "Cheng");
    }

    @Test
    public void getAge() {
        Assert.assertSame(p1.getAge(), 31);
    }

    @Test
    public void setForename() {
        p1.setForename("Sven");
        Assert.assertSame(p1.getForename(), "Sven");
    }

    @Test
    public void setLastname() {
        p1.setLastname("Sejdiu");
        Assert.assertSame(p1.getLastname(), "Sejdiu");
    }

    @Test
    public void setAge() {
        p1.setAge(32);
        Assert.assertSame(p1.getAge(), 32);
    }
}