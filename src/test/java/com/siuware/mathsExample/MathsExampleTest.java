package com.siuware.mathsExample;

import org.junit.Assert;
import org.junit.Test;

public class MathsExampleTest {

    @Test
    public void plus0() {
        Assert.assertEquals(MathsExample.plus(1, 1), 2);
    }

    @Test
    public void plus1() {
        Assert.assertEquals(MathsExample.plus(-1, 1), 0);
    }

    @Test
    public void minus0() {
        Assert.assertEquals(MathsExample.minus(1, 1), 0);
    }

    @Test
    public void minus1() {
        Assert.assertEquals(MathsExample.minus(1, 2), -1);
    }
}