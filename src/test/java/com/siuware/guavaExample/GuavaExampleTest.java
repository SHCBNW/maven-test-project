package com.siuware.guavaExample;

import com.siuware.lombockExample.PersonExample;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GuavaExampleTest {
    PersonExample p1;
    PersonExample p2;

    @Before
    public void setUp() throws Exception {
        p1 = new PersonExample("Siu", "Cheng", 31);
        p2 = new PersonExample("Julia", "Imhof", 30);
    }

    @Test
    public void compareTo1() {
        // 0 = equal
        Assert.assertEquals(0, GuavaExample.compareTo(p1, p1));
    }

    @Test
    public void compareTo2() {
        // 0 = equal
        Assert.assertNotEquals(0, GuavaExample.compareTo(p1, p2));
        ;
    }
}